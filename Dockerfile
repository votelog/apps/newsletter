# syntax=docker/dockerfile:1

# start from [official listmonk Docker image](https://hub.docker.com/r/listmonk/listmonk/)
FROM listmonk/listmonk:v4.1.0

# wipe existing `config.toml` (we use env vars instead)
# NOTE: the empty `config.toml` simply allows to run listmonk without specifying `--config ''`
RUN rm -f /listmonk/config.toml && touch /listmonk/config.toml

# add our public folder
COPY --link public /listmonk/public

# ignored by Fly
VOLUME [ "/listmonk/uploads" ]
