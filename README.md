# VoteLog newsletter

[![Health](https://status.votelog.ch/api/v1/endpoints/internal-tools_newsletter-(listmonk)/health/badge.svg)](https://status.votelog.ch/endpoints/internal-tools_newsletter-(listmonk))
[![Uptime (30 days)](https://status.votelog.ch/api/v1/endpoints/internal-tools_newsletter-(listmonk)/uptimes/30d/badge.svg)](https://status.votelog.ch/endpoints/internal-tools_newsletter-(listmonk))
[![Response time (30 days)](https://status.votelog.ch/api/v1/endpoints/internal-tools_newsletter-(listmonk)/response-times/30d/badge.svg)](https://status.votelog.ch/endpoints/internal-tools_newsletter-(listmonk))

This repository contains non-sensitive code and configuration related to the VoteLog mailing list tool available under
[**`newsletter.votelog.ch`**](https://newsletter.votelog.ch/).

We rely on [**listmonk**](https://listmonk.app/) which we host on [Fly](https://fly.io/).

Sensitive configuration data is stored via [Fly secrets](https://fly.io/docs/reference/secrets/). Currently this includes the following environment variables:

-   `LISTMONK_ADMIN_USER` (to be unset again after initial deployment)
-   `LISTMONK_ADMIN_PASSWORD` (to be unset again after initial deployment)
-   `LISTMONK_db__password`

## Hosting details

-   The Fly [app](https://fly.io/docs/reference/apps/) is named `votelog-newsletter` and has a [persistent storage
    volume](https://fly.io/docs/reference/volumes/) of 1 GiB size[^1] attached to it named `votelog_newsletter` with unique ID `vol_7r1zd9mn7gpg703v`. It
    currently runs on a single [`shared-cpu-1x` instance with `256 MB` RAM](https://fly.io/docs/about/pricing/#compute) and is hosted in the [*Paris, France*
    (`cdg`)](https://fly.io/docs/reference/regions/) region.
-   The `votelog-newsletter` app connects to the PostgreSQL database `listmonk` on our [Neon.tech](https://neon.tech/docs/introduction/about) account.
-   Should it prove necessary to expand performance for our main users (located in Switzerland), we could [increase
    RAM](https://fly.io/docs/flyctl/scale-memory/) and/or [switch to a faster CPU](https://fly.io/docs/flyctl/scale-vm/) anytime[^2].
-   Should we also want to provide fast access for non-European users, we could [run multiple instances](https://fly.io/docs/apps/scale-count/) of Listmonk [in
    multiple regions](https://fly.io/docs/apps/scale-count/#scale-an-app-s-regions).

[^1]: The volume size can always be [extended](https://fly.io/docs/flyctl/volumes-extend/). To extend it to 5 GiB for example, simply run:

    ``` sh
    flyctl volumes extend vol_7r1zd9mn7gpg703v --size=5
    ```

    Note that this will restart the app.

[^2]: Note that scaling automatically restarts the app. The most relevant documentation on (auto)scaling Fly apps includes:

    -   [Scale Machine CPU and RAM](https://fly.io/docs/apps/scale-machine/)
    -   [Scale the Number of Machines](https://fly.io/docs/apps/scale-count/)
    -   [Automatically Stop and Start Machines](https://fly.io/docs/apps/autostart-stop/)

## Admin access

We can directly connect into the root filesystem of the `votelog-newsletter` app via an [SSH tunnel](https://fly.io/docs/flyctl/ssh-console/) to perform any
low-level administration tasks. To log in over SSH, run:

``` sh
flyctl ssh console
```

## First-time setup

To deploy the very first `votelog-newsletter` release on Fly, alter the `deploy.release_command` in `fly.toml` to
`./listmonk --install --idempotent --yes --config ''` and run `flyctl deploy --ha=false`. Afterwards, revert the `deploy.release_command` to
`./listmonk --upgrade --yes --config=''`.

## Deploy new listmonk release

To deploy a new `votelog-newsletter` release on Fly, simply set the desired [`listmonk/listmonk` tag](https://hub.docker.com/r/listmonk/listmonk/tags) in
[`fly.toml`](fly.toml) and run

``` sh
flyctl deploy --ha=false
```

Possible database migrations are automatically run via Fly's
[`deploy.release_command`](https://fly.io/docs/reference/configuration/#run-one-off-commands-before-releasing-a-deployment) feature.

In case deployment of a new listmonk version fails, it's recommended to inspect a slightly modified VM via:

1.  Comment out the `services.tcp_checks` section in `fly.toml`.

2.  Overwrite the Dockerfile `CMD` by adding the following line to `Dockerfile`:

    ``` dockerfile
    CMD sleep infinity
    ```

3.  Run `flyctl deploy --ha=false`.

4.  Run `flyctl ssh console` to connect via SSH into the VM.

Then try to run the release command manually, i.e. `./listmonk --install --idempotent --yes && ./listmonk --upgrade --yes` and confirm the output doesn't
indicate any problems. Afterwards, try to launch listmonk via `./listmonk`. If it can run successfully, it should now be reachable on
<https://newsletter.votelog.ch/>.

If everything appears to work fine, revert steps 1 and 2 above and run `flyctl deploy` again.

## License

Code and configuration in this repository are licensed under [`AGPL-3.0-or-later`](https://spdx.org/licenses/AGPL-3.0-or-later.html). See
[LICENSE.md](LICENSE.md).

----------------------------------------------------------------------------------------------------------------------------------------------------------------
