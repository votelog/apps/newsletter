/* Initialize 'listmonk' database and user

   This script is intended to be run from Neon console, API or CLI on a freshly created DB 'listmonk'.
 */

-- Create user for listmonk
REASSIGN OWNED BY "listmonk" TO "admin";
DROP OWNED BY "listmonk";
DROP ROLE IF EXISTS "listmonk";
CREATE ROLE "listmonk" WITH LOGIN PASSWORD 'REPLACE-ME' ROLE "admin";
GRANT ALL PRIVILEGES ON DATABASE "listmonk" TO "listmonk";
GRANT ALL PRIVILEGES ON SCHEMA "public" TO "listmonk"; /* This needs to be run from the Neon console, CLI or API to take effect! */
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA "public" TO "listmonk";
GRANT ALL PRIVILEGES ON ALL ROUTINES IN SCHEMA "public" TO "listmonk";
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA "public" TO "listmonk";
ALTER DEFAULT PRIVILEGES IN SCHEMA "public" GRANT ALL PRIVILEGES ON SEQUENCES TO "listmonk";
ALTER DEFAULT PRIVILEGES IN SCHEMA "public" GRANT ALL PRIVILEGES ON ROUTINES TO "listmonk";
ALTER DEFAULT PRIVILEGES IN SCHEMA "public" GRANT ALL PRIVILEGES ON TABLES TO "listmonk";
